<?php

/**
 * Wrapper function to output featured image
 * 
 * $args is an array of parameters:
 * 'size' => 'full' || 'thumb' || 'large'
 * 'url'  => 'http://images.jpg'
 * 'id'   => post id to pull image from
 *
 * @return string  URL of featured image
 * @author dev@duable.com
 **/
function du_featured_image( array $args = array() ) {
  $featured_image = new du\featured_image( $args );
  return $featured_image->get_image();
}