# README #

Helper functions to output stored images.

### What is this repository for? ###

* This is meant for developers who want more control over featured image output.

### How do I get set up? ###

Installation:

* Composer
* Upload to plugins directory
* Include files in functions.php file

### Usage ###

Output featured image:

```php

$args = array (
  'size' => 'full' || 'thumb' || 'large',
  'url'  => 'http://images.jpg',
  'id'   => post id to pull image from,
  'parent_post' => If no featured image on current post, use parent's? true||false
);

<img src="echo du_featured_image( $args );" />

```

You can overwrite the image pulled by using the du_featured_image filter:

```php

add_filter( 'du_featured_image', 'featured_image_overwrite' );

# Replace your-template-file.php with the name of your landing page template
function featured_image_overwrite( $image_source ) {
  $image_source = 'path-to-image.jpg';
  return $image_source;
}
```

### Who do I talk to? ###

Developed by duable.com