<?php 
/*
Plugin Name: Du Featured Image
Plugin URI: https://duable.com/
Description: Output featured images with a few more options than WordPress offers out of the box.
Version: 0.9
Author: Duable <dev@duable.com>
Author URI: https://duable.com/
*/

namespace du;

# Include wrapper functions
include_once( __DIR__ . '/functions.php' );

/**
 * Find and output featured image
 *
 * @filter du_featured_image sets an image url to show
 * @package du
 * @author dev@duable.com
 **/
class featured_image {
  
  /**
   * Default image settings
   * size: image size to work with
   * parent: pull the current post's parent featured image
   */
  private $image_defaults = array(
    'crop'        => 'c' , 
    'size'        => 'full',
    'parent_post' => false
  );
  
  public function __construct( array $args = array() ){
    # Merge defaults and passed variables
    $args = array_merge( $this->image_defaults , $args ) ;
    
    # Let's make all the arguments available to the class
    foreach ( $args as $arg => $value ) {
      $this->$arg = $value;
    }
  }

  /*
  		Return, pull, and set images for featured images, slider images, background, etc.
  */
  public function get_image() {
    /**
     * If a post is passed to the function, let's pull that posts thumbnails
     * Otherwise, let's use the current view's post object
     */
    if ( isset( $this->id ) ) :
      $post = get_post( $this->id );
    else :
      global $post;
    endif;

    if ( isset( $post->ID ) ) {
      # Set $the_post to either current or parent post object
      $this->the_post = ( $this->parent_post ? $post->post_parent : $post->ID );

      # Get thumbnail (featured image) id
      $thumbnail_id = get_post_thumbnail_id( $this->the_post );

      # Check if we have a url for an image passed
      if ( !empty( $this->url ) ) :
        $this->image_source = apply_filters( 'du_featured_image', $this->image_source );
        return $this->image_source;
  
      
      # Check if post has a featured image set
      elseif ( !empty( $thumbnail_id ) ) :
        $this->image_source = wp_get_attachment_image_src( $thumbnail_id, $this->size );
        $this->image_source = $this->image_source[0];
  
        # Allow themes and plugins to filter the image source
        $this->image_source = apply_filters( 'du_featured_image', $this->image_source );
        return $this->image_source;
  
      else :
        /**
         * No featured image for the current post and no url set, 
         * so let's check if the results are being filtered by 
         * themes/plugins.  If not, return false.
         */
        $this->url = ( !empty( $this->url ) ? $this->url : null );
        $this->image_source = apply_filters( 'du_featured_image', $this->url );
        return ( empty( $this->image_source ) ? false : $this->image_source );
      endif;
    }
  }
}